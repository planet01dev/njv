<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/auth/login', 301);

Route::get('/signup', function () {
     return view('auth/register');
});

Route::get('/sign-in', function () {
    return view('website//customer/signin');
});
;
Route::get('/', function () {
    return view('auth/login');
});
Route::get('/login', function () {
    return view('auth/login');
});
Route::post('/login', 'Auth\LoginController@login')->name('login')->middleware("throttle:3,1");
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'Admin\DashboardController@manage')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin', 'auth', 'checkType'],], function () {
	//home
	Route::get('/', 'Admin\UserController@manage')->name('admin.home');
});
Route::group(['middleware' => ['auth', 'checkType'],], function () {
	//dashboard
	Route::group(['prefix' => 'dashboard'], function () {
		Route::get('/', 'Admin\DashboardController@manage')->name('admin.dashboard.manage');
		Route::get('add', 'Admin\DashboardController@add')->name('admin.dashboard.add');
	   Route::get('edit/{id}', 'Admin\DashboardController@edit')->name('admin.dashboard.edit');
	   Route::post('post', 'Admin\DashboardController@post')->name('admin.dashboard.post');
	   Route::post('delete/{id}', 'Admin\DashboardController@delete')->name('admin.dashboard.delete');
	});
	//user
 	Route::group(['prefix' => 'user'], function () {
 		Route::get('/', 'Admin\UserController@manage')->name('admin.user.manage');
 		Route::get('add', 'Admin\UserController@add')->name('admin.user.add');
		Route::get('edit/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
		Route::post('post', 'Admin\UserController@post')->name('admin.user.post');
		Route::post('delete/{id}', 'Admin\UserController@delete')->name('admin.user.delete');
 	});
 	//device
 	Route::group(['prefix' => 'device'], function () {
 		Route::get('/', 'Admin\DeviceController@manage')->name('admin.device.manage');
 		Route::get('add', 'Admin\DeviceController@add')->name('admin.device.add');
		Route::get('edit/{id}', 'Admin\DeviceController@edit')->name('admin.device.edit');
		Route::post('post', 'Admin\DeviceController@post')->name('admin.device.post');
		Route::post('delete/{id}', 'Admin\DeviceController@delete')->name('admin.device.delete');
		Route::get('/detail/{id}', 'Admin\DeviceController@detail')->name('admin.device.detail');

		Route::get('/import', 'Admin\DeviceController@getImport')->name('admin.device.import');
		Route::post('/import_parse', 'Admin\DeviceController@parseImport')->name('admin.device.import_parse');
		Route::post('/import_process', 'Admin\DeviceController@processImport')->name('admin.device.import_process');
		Route::get('/export', 'Admin\DeviceController@export')->name('admin.device.export');
		Route::get('single-device-export\{id}', 'Admin\DeviceController@single_device_report')->name('admin.device.single.report');
	 });
	 
	 //device history
 	Route::group(['prefix' => 'device_history'], function () {
		Route::get('/', 'Admin\DeviceHistoryController@manage')->name('admin.device_history.manage');
		Route::get('add', 'Admin\DeviceHistoryController@add')->name('admin.device_history.add');
	   Route::get('edit/{id}', 'Admin\DeviceHistoryController@edit')->name('admin.device_history.edit');
	   Route::post('post', 'Admin\DeviceHistoryController@post')->name('admin.device_history.post');
	   Route::post('delete/{id}', 'Admin\DeviceHistoryController@delete')->name('admin.device_history.delete');

	});
	//cities
	Route::group(['prefix' => 'city'], function () {
		Route::get('/', 'Admin\CityController@manage')->name('admin.city.manage');
		Route::get('add', 'Admin\CityController@add')->name('admin.city.add');
	   Route::get('edit/{id}', 'Admin\CityController@edit')->name('admin.city.edit');
	   Route::post('post', 'Admin\CityController@post')->name('admin.city.post');
	   Route::post('delete/{id}', 'Admin\CityController@delete')->name('admin.city.delete');
	});
	//schools
	Route::group(['prefix' => 'school'], function () {
		Route::get('/', 'Admin\SchoolController@manage')->name('admin.school.manage');
		Route::get('add', 'Admin\SchoolController@add')->name('admin.school.add');
	   Route::get('edit/{id}', 'Admin\SchoolController@edit')->name('admin.school.edit');
	   Route::post('post', 'Admin\SchoolController@post')->name('admin.school.post');
	   Route::post('delete/{id}', 'Admin\SchoolController@delete')->name('admin.school.delete');
	});

});

//manager routes
Route::group(['prefix' => 'manager', 'middleware' => ['role:manager', 'auth', 'checkType'],], function () {
 		Route::get('/', 'Admin\UserController@manage')->name('manager.home');
});
//viewers routes
Route::group(['prefix' => 'viewer', 'middleware' => ['role:viewer', 'auth', 'checkType'],], function () {
 	Route::get('/', 'Admin\UserController@manage')->name('viewer.home');
});
//superadmin routes 
Route::group(['prefix' => 'superadmin', 'middleware' => ['role:superadmin', 'auth'],], function () {
	//roles
    Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');
	Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('assign.role');

});