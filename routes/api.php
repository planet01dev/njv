<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('/cruds', 'CrudsController', [
  'except' => ['edit', 'show', 'store']
]);

Route::group([
'prefix' => 'device'
], function () {
    Route::post('post_device', 'Api\DeviceController@post_device');
    Route::post('post_history', 'Api\DeviceController@post_history');
});
Route::group([
'prefix' => 'cities'
], function () {
    Route::get('/', 'Api\DeviceController@cities');
});
Route::group([
'prefix' => 'schools'
], function () {
    Route::get('/', 'Api\DeviceController@schools');
});


