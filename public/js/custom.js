function initComp(){
    $('.ajaxFormSubmit').on('click',function(e){
        var form = $('form.ajaxForm');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        form.submit();

       /* swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });*/
    });

    $("form.ajaxForm").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            $(".loading-wrapper").show();
            $("#loader").show();
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();
            $(".loading-wrapper").hide();
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            error(value);
                        });
                    }/*else{
                        error('Something went wrong. Please try again later.');
                    }*/
                });
            }else if(data.status === 500){
                error('Something went wrong. Please try again later.');
            }else if(typeof data === 'object'){
               /* $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");
                $('.back-to-top').click();*/
               error("Please fill all mandatory fields.");   
            }else{
                /*$('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();*/
             error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
               /* $('#fail').show();
                $('#fail-text').html(data['error']);
                $('.back-to-top').click();*/
                $("#loader").hide();
                //$(".loading-wrapper").hide();
            }
            else if (data['success'] !== undefined){
                $("#loader").hide();
               /* $(".loading-wrapper").hide();
                $('#success').show();
                $('#success-text').html(data['success']);*/
                success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){
                resetForm();

            }
        }
    });
}
$(document).ready(function() {
    var form = $('.ajaxForm'),
    original = form.serialize()

    form.submit(function(){
      window.onbeforeunload = null
    })

    /*window.onbeforeunload = function(){
      if (form.serialize() != original)
        return 'Are you sure you want to leave?'
    }*/

    initComp();
});
function resetForm(){
    $("form input[type=text]").val("");

    $("form input[type=password]").val("");

    $("form input[type=email]").val("");

    $("form input[type=color]").val("");

    $("form input[type=date]").val("");

    $("form input[type=datetime-local]").val("");

    $("form input[type=file]").val("");

    $("form input[type=image]").val("");

    $("form input[type=month]").val("");

    $("form input[type=number]").val("");

    $("form input[type=range]").val("");

    $("form input[type=tel]").val("");

    $("form input[type=url]").val("");

    $("form input[type=week]").val("");

    $("form select").val("");

    $("form textarea").val("");

}

function error(message)
{
    delay(200, function() {
        return toastr.error(message, 'Error');
    });
}
function success(message)
{
    delay(200, function() {
        return toastr.success(message, 'Success');
    });
}
delay = function(ms, func) {
   return setTimeout(func, ms);
};
toastr.options = {
  positionClass: 'toast-bottom-left'
};
function item_delete(id, this_url){      
    swal({
          title: "Do you really want to delete it?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                        url: this_url,
                        type: "post",
                        data: {
                            id:id
                        },
                        success: function(data) {
                            data = JSON.parse(data);
                            if (data.status == 'success') {
                                success(data.data);
                                setTimeout(
                                  function() 
                                  {
                                    location. reload(true);
                                  }, 1000);                       
                            }else if(data.status == 'error'){
                                error(data.data);
                            }
                        },
                        error: function() {
                            error('There is error while deleting record');
                        }
                    }); 
          } else {

            return false;
          }
        });
}