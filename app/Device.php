<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Device extends Model {

	//protected $fillable = ['device_number'];

	public function device_history(){
        return $this->hasMany('App\DeviceHistory', 'device_id', 'id');
    }
    public function history(){
        return $this->hasOne('App\DeviceHistory', 'device_id', 'id');
    }
    public function city(){
         return $this->belongsTo('App\City', 'city_id');
    }
    public function school(){
         return $this->belongsTo('App\School', 'school_id');
    }

   
}

