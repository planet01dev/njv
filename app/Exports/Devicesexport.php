<?php

namespace App\Exports;

use App\Device;
use Maatwebsite\Excel\Concerns\FromCollection;

class DevicesExport implements FromCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection()
    {
        return Device::all();
    }
}
