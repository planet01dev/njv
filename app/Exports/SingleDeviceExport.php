<?php 
namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SingleDeviceExport implements FromView
{   
	public $data;
	public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        return view('admin.device.single_device_export', [
            'data' => $this->data
        ]);
    }
}