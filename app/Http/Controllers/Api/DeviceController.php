<?php
namespace App\Http\Controllers\api;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Device;
use App\City;
use App\School;
use App\DeviceHistory;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
class DeviceController extends Controller
{

    public function post_device(Request $request){
        $request->validate([
            'name' => ['required'],
            'father_name' => ['required'],
            'phone' => ['required'],
            'city' => ['required'],
            'province' => ['required'],
            'country' => ['required'],
            'school' => ['required'],
            'student_class' => ['required'],
            'student_group' => ['required'],
            'section' => ['required'],
            'board' => ['required'],
        ]);
        $data = new Device();
        $data->device_number = $request->device_number;
        $data->name = $request->name;
        $data->father_name = $request->father_name;
        $data->phone = $request->phone;
        $data->city = $request->city;
        $data->province = $request->province;
        $data->country = $request->country;
        $data->school = $request->school;
        $data->student_class = $request->student_class;
        $data->student_group = $request->student_group;
        $data->section = $request->section;
        $data->board = $request->board;
        $data->save();      

        return response()->json([
            'status' => 'success',
            'id'     => $data->id,
            'message' => 'Succesfully Inserted',
        ]);
    } 

    public function post_history(Request $request){
        /*$array = [
            "app" =>[
                [
                    'latitude' => 4.56,
                    'longitude' => 4.56,
                    'app_name' => 4.56,
                    'minutes' => 90,
                ],
                [
                    'latitude' => 4.56,
                    'longitude' => 4.56,
                    'app_name' => 4.56,
                    'minutes' => 90,
                ],
                [
                    'latitude' => 4.56,
                    'longitude' => 4.56,
                    'app_name' => 4.56,
                    'minutes' => 90,
                ],
            ]
        ];
        return response()->json(['mac_address' => '567', 'data' => $array]);*/
        $request->validate([
            'mac_address' => ['required']
        ]);
        $get_id = 0;
        $find = Device::where('device_mac_address', $request->mac_address)->first();
        if($find){
           $get_id = $find->id;
        }else{
            $data = new Device();
            $data->device_mac_address = $request->mac_address;
            $data->save();
            $get_id = $data->id;
        }

        if (isset($request->data['app'])) {
            $app = $request->data['app'];
            for ($k = 0; $k< sizeof($app); $k++) {
                $detail_data = new DeviceHistory();
                $detail_data->device_id = $get_id;
                $detail_data->mac_address = @$request->mac_address;
                
                $detail_data->last_successful_data_upload_time = @$request->last_successful_data_upload_time;
                $detail_data->contacts_permission = @$request->contacts_permission;
                $detail_data->location_permission = @$request->location_permission;
                $detail_data->device_admin_permission = @$request->device_admin_permission;
                $detail_data->usage_status_permission = @$request->usage_status_permission;
                $detail_data->primary_email_of_device = @$request->primary_email_of_device;

                $detail_data->latitude = @$app[$k]['latitude'];
                $detail_data->longitude = @$app[$k]['longitude'];
                $detail_data->app_name = @$app[$k]['app_name'];
                $detail_data->minutes = @$app[$k]['minutes'];
                $detail_data->save();
            }
            return response()->json([
                'status' => 'success',
                'message' => 'Succesfully Inserted',
            ]);
        }
        return response()->json([
            'status' => 'failed',
            'message' => 'Something went wrong',
        ]);

    } 

    public function cities()
    {
        $data = City::get();
        return response()->json([
            'status' => 'success',
            'data' => $data,
        ]);
    } 
    public function schools()
    {
        $data = School::get();
        return response()->json([
            'status' => 'success',
            'data' => $data,
        ]);
    } 



}
