<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\School;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{
    
    public function manage()
    {   
        $data = School::get();
        return view('admin.school.manage')->with(['title' =>  'School', 'data' => $data]);
    }

    public function add(Request $request){
        if(!is_admin()){
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        return view('admin.school.edit')->with(['title' => 'Add']);   
    }

    public function edit(Request $request){
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        $data = School::where('id', $request->id)->first();
        return view('admin.school.edit')->with(['title' => 'Edit','data' => $data,]);   
    }

    public function post(Request $request){
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        if(isset($request->id) && !empty($request->id)){
            $request->validate([
                'title' => ['required'],
            ]);
            $find = School::where('title', $request->title)->where('id', '!=', $request->id)->first();
            if ($find) {
                return ["error" => 'The School name has already been taken'];
            }
            
            $data =[
                'title' => $request->title,                
            ];
            
            //update data
            $dt = School::where('id', $request->id)->update($data);
            
            
            return ["success" => 'Successfully updated', "redirect" => route('admin.school.manage')];
        }else{ 
             $request->validate([
                'title' => ['required'],
             ]);
            $find = School::where('title', $request->title)->first();
            if ($find) {
                return ["error" => 'The school name has already been taken'];
            }
            $data = new School();
            $data->title = $request->title;
            $data->save();

            return ["success" => 'Successfully added', "redirect" => route('admin.school.manage')];
        }
    }

    public function delete(Request $request){
      if(!is_admin() && !is_manager()){
        echo json_encode(['status' => 'error', 'data' => permission_denied()], true);
      }else{
        $id = $request->id;
        School::where('id', $id)->delete();
        echo json_encode(['status' => 'success', 'data' => 'Successfully deleted']);
      }
    }

  



}
