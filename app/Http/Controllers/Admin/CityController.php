<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class CityController extends Controller
{
    
    public function manage()
    {   
        $data = City::get();
        return view('admin.city.manage')->with(['title' =>  'Cities', 'data' => $data]);
    }

    public function add(Request $request){
        if(!is_admin()){
            return Redirect::back()->withErrors(['msg', permisssion_denied()]);
        }
        return view('admin.city.edit')->with(['title' => 'Add']);   
    }

    public function edit(Request $request){
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors(['msg', permisssion_denied()]);
        }
        $data = City::where('id', $request->id)->first();
        return view('admin.city.edit')->with(['title' => 'Edit','data' => $data,]);   
    }

    public function post(Request $request){
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors(['msg', permisssion_denied()]);
        }
        if(isset($request->id) && !empty($request->id)){
            $request->validate([
                'title' => ['required'],
            ]);
            $find = City::where('title', $request->title)->where('id', '!=', $request->id)->first();
            if ($find) {
                return ["error" => 'The City name has already been taken'];
            }
            
            $data =[
                'title' => $request->title,                
            ];
            
            //update data
            $dt = City::where('id', $request->id)->update($data);
            
            
            return ["success" => 'Successfully updated', "redirect" => route('admin.city.manage')];
        }else{ 
             $request->validate([
                'title' => ['required'],
             ]);
            $find = City::where('title', $request->title)->first();
            if ($find) {
                return ["error" => 'The city name has already been taken'];
            }
            $data = new City();
            $data->title = $request->title;
            $data->save();

            return ["success" => 'Successfully added', "redirect" => route('admin.city.manage')];
        }
    }

    public function delete(Request $request){
      if (!is_admin() && !is_manager()) {
         echo json_encode(['status' => 'error', 'data' => permission_denied()], true);
      }else{
        $id = $request->id;
        City::where('id', $id)->delete();
        echo json_encode(['status' => 'success', 'data' => 'Successfully deleted']);
      }
      
    }

  



}
