<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Device;
use App\City;
use App\School;
use App\DeviceHistory;
use App\Http\Requests\CsvImportRequest;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\DevicesImport;
use App\Exports\DevicesExport;
use App\Exports\SingleDeviceExport;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class DeviceController extends Controller
{

    public function manage(Request $request)
    {
        $data = Device::with(['device_history', 'school', 'city']);
        $filter = [];
        if(isset($request->country) && $request->country){
            $filter['country'] = $request->country;
            $data = $data->where('country_id', $request->country);
        }
        if(isset($request->school) && $request->school){
            $filter['school'] = $request->school;
            $data = $data->where('school_id', $request->school);
        }
        $data = $data->paginate(25);
        $cities  = City::get();
        $schools  = School::get();
        return view('admin.device.manage')->with(['title' =>  'Devices', 'data' => $data, 'cities' => $cities, 'schools' => $schools, 'filter' => $filter]);
    }

    public function add(Request $request)
    {
        if (!is_admin()) {
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $cities = City::get();
        $schools = School::get();
        return view('admin.device.edit')->with(['title' => 'Add', 'cities' => $cities, 'schools' => $schools]);
    }

    public function edit(Request $request)
    {
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $data = Device::where('id', $request->id)->first();
        $detail_data = DeviceHistory::where('device_id', $request->id)->get();
        $cities  = City::get();
        $schools = School::get();
        return view('admin.device.edit')->with(['title' => 'Edit', 'data' => $data, 'detail_data' => $detail_data, 'cities' => $cities, 'schools' => $schools]);
    }

    public function post(Request $request)
    {
        if (!is_admin() && !is_manager()) {
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $request->validate([
            'device_number' => ['required', 'string', 'max:255'],
            'device_mac_address' => ['required', 'string', 'max:255'],
            'name' => ['required'],
        ]);
        if (isset($request->id) && !empty($request->id)) {
            $find = Device::where('id', '!=',$request->id)->where('device_number', $request->device_number)->first();
            if ($find) {
                return ["error" => 'The device number has already been taken'];
            }
            $find = Device::where('id', '!=' ,$request->id)->where('device_mac_address', $request->device_mac_address)->first();
            if ($find) {
                return ["error" => 'The mac address has already been taken'];
            }

            $device_data = [
                'device_number' => $request->device_number,
                'device_mac_address' => $request->device_mac_address,
                'name' => $request->name,
                'father_name' => $request->father_name,
                'phone' => $request->phone,
                'city_id' => $request->city_id,
                'province' => $request->province,
                'country' => $request->country,
                'school_id' => $request->school_id,
                'student_class' => $request->student_class,
                'student_group' => $request->student_group,
                'section' => $request->section,
                'board' => $request->board,
            ];


            //update data
            $data = Device::where('id', $request->id)->update($device_data);

            DeviceHistory::where('device_id', $request->id)->delete();

            if (isset($request->mac_address)) {
                $mac_address = $request->mac_address;
                for ($k = 0; $k < sizeof($mac_address); $k++) {
                    $data = new DeviceHistory();
                    $data->device_id = $request->id;
                    $data->mac_address = $request->mac_address[$k];
                    $data->latitude = $request->latitude[$k];
                    $data->longitude = $request->longitude[$k];
                    $data->app_name = $request->app_name[$k];
                    $data->minutes = $request->minutes[$k];
                    $data->save();
                }
            }


            return ["success" => 'Successfully updated', "redirect" => route('admin.device.manage')];
        } else {
            $request->validate([
                'device_number' => ['required', 'max:255', 'unique:devices'],
                'device_mac_address' => ['required', 'max:255', 'unique:devices'],
                'name' => ['required'],
            ]);
            $data = new Device();
            $data->device_mac_address = $request->device_mac_address;
            $data->device_number = $request->device_number;
            $data->name = $request->name;
            $data->father_name = $request->father_name;
            $data->phone = $request->phone;
            $data->city_id = $request->city_id;
            $data->province = $request->province;
            $data->country = $request->country;
            $data->school_id = $request->school_id;
            $data->student_class = $request->student_class;
            $data->student_group = $request->student_group;
            $data->section = $request->section;
            $data->board = $request->board;

            $data->save();

            if (isset($request->mac_address)) {
                $mac_address = $request->mac_address;
                for ($k = 0; $k < sizeof($mac_address); $k++) {
                    $detail_data = new DeviceHistory();
                    $detail_data->device_id = $data->id;
                    $detail_data->mac_address = $request->mac_address[$k];
                    $detail_data->latitude = $request->latitude[$k];
                    $detail_data->longitude = $request->longitude[$k];
                    $detail_data->app_name = $request->app_name[$k];
                    $detail_data->minutes = $request->minutes[$k];
                    $detail_data->save();
                }
            }

            return ["success" => 'Successfully added', "redirect" => route('admin.device.manage')];
        }
    }

    public function detail(Request $request)
    {
        $data = Device::with(['device_history' => function($q){
                    $q->orderBy('created_at','DESC'); 
                }])->where('id', $request->id)->first();
        $data['detail_data'] = $data->device_history->groupBy(function($item) {
             return $item->created_at->format('Y-m-d');
        });
        return view('admin.device.detail')->with(['title' => 'Device Detail', 'data' => $data]);
    }

    public function delete(Request $request)
    {
        if (!is_admin() && !is_manager()) {
            return false;
        }
        $id = $request->id;
        Device::where('id', $id)->delete();
        return 'success';
    }


    public function getImport()
    {
        return view('admin.device.import');
    }

    public function parseImport(CsvImportRequest $request)
    {

        
        $new_file_name = md5(uniqid()) . '.' . $request->file('csv_file')->getClientOriginalExtension();
        $path = $request->file('csv_file')->move(storage_path() . '/app/', $new_file_name);
        $data = Excel::toCollection(new DevicesImport, $new_file_name);


        $csv_header_fields = [];
        if ($request->has('header')) {

            $csv_header_fields[] = $data[0][0];
            unset($data[0][0]);
        }
        // dd($data);
        $error_check = 0;
        $validator= array();
        foreach ($data as $k => $row) {
            foreach ($row as $k2 => $v2) {
                
                
                $device_number = Device::where('device_number', $v2[0])->first();
                if ($device_number || empty($v2[0])) {
                    $error_check = 1;
                    if(empty($v2[0]))
                    {
                        array_push($validator,'Device Number cannot be empty');
                    }
                    else if($device_number)
                    {
                        array_push($validator,'Device Number '.$v2[0].' already exist in db.');
                    }
                    
                }
            }
            
        }
        if($error_check == 1)
        {
            return Redirect::back()->withErrors($validator);
        }

        $csv_data = $data;

        $csv_data_file = array(
            'csv_filename' => $new_file_name,
            'csv_header' => $request->has('header'),
            'csv_data' => json_encode($data)
        );
        
        return view('admin.device.import_fields', compact('csv_header_fields', 'csv_data', 'csv_data_file'));
    }

    public function processImport(Request $request)
    {

        $csv_data = json_decode($request->csv_data);
        // dd($csv_data);
        foreach ($csv_data as $k => $row) {
            foreach ($row as $k2 => $v2) {
                $device_number = Device::where('device_number', $v2[0])->first();
                if (!$device_number && !empty($v2[0])) {
                    $data = new Device();
                    $data->device_number = $v2[0];
                    $data->device_mac_address = $v2[1];
                    $data->name = $v2[2];
                    $data->father_name = $v2[3];
                    $data->phone = $v2[4];
                    $data->city_id = $v2[5];
                    $data->province = $v2[6];
                    $data->country = $v2[7];
                    $data->school_id = $v2[8];
                    $data->student_class = $v2[9];
                    $data->student_group = $v2[10];
                    $data->section = $v2[11];
                    $data->board = $v2[12];
                    $data->save();
                }
            }
        }

        return view('admin.device.import_success');
    }

    public function export() 
    {
        return Excel::download(new DevicesExport, 'Devices.csv');
    }

    public function single_device_report(Request $request){
       if(isset($request->id)){
            $data = Device::with(['device_history'])->where('id', $request->id)->first();
            return Excel::download(new SingleDeviceExport($data), 'excel_report.csv');
        }       
    }
}
