<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    
    public function manage()
    {   
       
        $data = User::with('role')->where('id', '!=', Auth::user()->id)->get();
        return view('admin.user.manage')->with(['title' =>  'Users', 'data' => $data]);
    }

    public function add(Request $request){
        if(!is_admin()){
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        $role = UserRole::get();
        return view('admin.user.edit')->with(['title' => 'Add', 'role' => $role]);   
    }

    public function edit(Request $request){
        if(!is_admin() && $request->id != Auth::user()->id){
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        $data = User::where('id', $request->id)->first();
        $role = UserRole::get();
        return view('admin.user.edit')->with(['title' => 'Edit', 'data' => $data, 'role' => $role]);   
    }

    public function post(Request $request){
        if(!is_admin() && $request->id != Auth::user()->id){
            return Redirect::back()->withErrors([ permisssion_denied()]);
        }
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'user_type' => ['required'],
        ]);
        if(isset($request->id) && !empty($request->id)){
            $find_email = User::where('email', $request->email)->where('id', '!=', $request->id)->first();
            if($find_email){
                return ["error" => 'The email has already been taken'];
            }
            $user_data =[
                'name' => $request->name,
                'email' => $request->email, 
                'user_type' => $request->user_type,
            ];
            if(isset($request->password) && $request->password != ''){
                $request->validate([
                    'password' => ['required', 'string', 'min:12', 'confirmed'],
                ]);
                if(!password_pattren($request->password)){
                    return ["error" => 'Password should be minimum 12 characters long, with small/capital letters + digits'];
                }
                $user_data['password'] = Hash::make($request->password);

            }
            //remove previous role
            $user = User::where('id', $request->id)->first();
            $role = Role::where('id', '=', $user->user_type)->firstOrFail();
            $user->removeRole($role->name);
            //update data
            $data = User::where('id', $request->id)->update($user_data);
            //assign role
            $user = User::where('id', $request->id)->first();
            $role_r = Role::where('id', '=', $request->user_type)->firstOrFail();  
            $user->assignRole($role_r);


            return ["success" => 'Successfully updated', "redirect" => route('admin.user.manage')];
        }else{ 
            $request->validate([
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:12', 'confirmed'],
            ]);
            if(!password_pattren($request->password)){
                return ["error" => 'Password should be minimum 12 characters long, with small/capital letters + digits'];
            }
            $data = new User();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->user_type = $request->user_type;
            $data->password = Hash::make($request->password);
            $data->save();

            $role_r = Role::where('id', '=', $request->user_type)->firstOrFail();  
            $data->assignRole($role_r);
            
            return ["success" => 'Successfully added', "redirect" => route('admin.user.manage')];
        }
    }

    public function delete(Request $request){
      if (!is_admin()) {
         echo json_encode(['status' => 'error', 'data' => permission_denied()], true);
      }else{
        $id = $request->id;
        User::where('id', $id)->delete();
        echo json_encode(['status' => 'success', 'data' => 'Successfully deleted']);
      }
    }

  



}
