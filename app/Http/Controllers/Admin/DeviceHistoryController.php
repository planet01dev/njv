<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Device;
use App\DeviceHistory;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
class DeviceHistoryController extends Controller
{
    
    public function manage(Request $request)
    {   
        if(isset($request->device_id)){
            $date = $request->date; 
            $data = DeviceHistory::with(['device'])->where('device_id', $request->device_id)->where('created_at', '>=', $date." 00:00:00")->where('created_at', '<=', $date." 23:59:59")->paginate(25);
        }else{
            $data = DeviceHistory::with('device')->paginate(25);
        }
        return view('admin.device_history.manage')->with(['title' =>  'Devices History', 'data' => $data]);
    }

    public function add(Request $request){
        if(!is_admin()){
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $devices = Device::get();
        return view('admin.device_history.edit')->with(['title' => 'Add','devices'=>$devices]);   
    }

    public function edit(Request $request){
        if(!is_admin() && !is_manager()){
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $devices = Device::get();
        $data = DeviceHistory::where('id', $request->id)->first();
        return view('admin.device_history.edit')->with(['title' => 'Edit', 'devices'=>$devices, 'data' => $data,]);   
    }

    public function post(Request $request){
        if(!is_admin() && !is_manager()){
            return Redirect::back()->withErrors([permission_denied()]);
        }
        $request->validate([
            'device_id' => ['required'],
            'mac_address' => ['required'],
        ]);
        if(isset($request->id) && !empty($request->id)){
            
            $device_data =[
                'device_id' => $request->device_id,
                'mac_address' => $request->mac_address, 
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'app_name' => $request->app_name,
                'minutes' => $request->minutes,
                
            ];
            
            
            //update data
            $data = DeviceHistory::where('id', $request->id)->update($device_data);
            
            
            return ["success" => 'Successfully updated', "redirect" => route('admin.device_history.manage')];
        }else{ 
            $request->validate([
                'device_id' => ['required'],
                'mac_address' => ['required'],
            ]);
            
            $data = new DeviceHistory();
            $data->device_id = $request->device_id;
            $data->mac_address = $request->mac_address;
            $data->latitude = $request->latitude;
            $data->longitude = $request->longitude;
            $data->app_name = $request->app_name;
            $data->minutes = $request->minutes;
            $data->save();

            return ["success" => 'Successfully added', "redirect" => route('admin.device_history.manage')];
        }
    }

    public function delete(Request $request){
      if(!is_admin() && !is_manager()){
        echo json_encode(['status' => 'error', 'data' => permission_denied()], true);
      }else{
          $id = $request->id;
          DeviceHistory::where('id', $id)->delete();
          echo json_encode(['status' => 'success', 'data' => 'Successfully deleted']);
      }
    }

  



}
