<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use App\Device;
use App\User;
use App\School;
use App\DeviceHistory;
use App\Role as UserRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
class DashboardController extends Controller
{
    
    public function manage(Request $request)
    {   
        $devices = Device::get()->count();
        $cities  = City::get()->count();
        $schools = School::get()->count();
        $users = User::get()->count();
        $range   = ['oneday' => 1, 'lastweek' => 7, 'month' =>30,  'all' => 0];
        $history = DeviceHistory::orderBy('minutes', 'desc');
        if(isset($request->from) && $request->from){
            if($request->from != 'all' && array_key_exists($request->from, $range)){
                 $history = $history->where('created_at', '>=', Carbon::now()->subDay($range[$request->from]));
            }
        }else{
            $history = $history->where('created_at', '>=', Carbon::now()->subDay($range['oneday']));
        }
        $history = $history->groupBy('app_name')->take(10)->get();
        $recent_data = DeviceHistory::where('created_at', '>=', Carbon::now()->subDay(1))->get()->count();
        $loc = Device::with(['history' => function($q){
                    $q->orderBy('created_at','DESC'); 
                }])->get();
        $location = array();
        $j=1;
        foreach($loc as $v){
            if($v->history){
                $location []=  [$v->name, $v->history->latitude, $v->history->longitude, $j];
                $j++;
            }
        }
        
        return view('admin.dashboard.manage')->with(['title' =>  'Dashboard', 'devices' => @$devices,'cities' => @$cities, 'schools' => @$schools, 'users' => @$users,'history' => $history, 'recent_data' => $recent_data, 'location' => json_encode($location)  ]);
    }

   

}
