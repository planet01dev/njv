<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    

    protected function authenticated($request, $user){
        $user->last_login_at = $user->updated_at;
        $user->updated_at = Carbon::now()->toDateTimeString();
        $user->save();
        if($user->hasRole('admin')){
            return redirect(route('admin.home'));
        }else if($user->hasRole('manager')){
            return redirect(route('manager.home'));
        }else if($user->hasRole('viewer')){
            return redirect(route('viewer.home'));
        } else {
            return redirect('/');
        }
    }
}
