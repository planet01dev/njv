<?php
use App\User;
function dt_format($date) {
   return date("M-d-Y", strtotime($date));
}

function dt_time_format($date) {
    $dt   = date("M-d-Y", strtotime($date));
    $time = date("H:i:s", strtotime($date));
   return [$dt, $time];
}
function password_pattren($password){
  return (preg_match('(^(?=.*[a-z]|[A-Z])(?=.*\d).+$)', $password) && strlen($password) > 11)? true : false;
}
function is_admin(){
	$user = User::where('id',Auth::user()->id)->first();
	if($user->hasRole('admin')){
		return true;
	}else{
		return false;
	}
}
function is_manager(){
	$user = User::where('id',Auth::user()->id)->first();
	if($user->hasRole('manager')){
		return true;
	}else{
		return false;
	}
}
function is_viewer(){
	$user = User::where('id',Auth::user()->id)->first();
	if($user->hasRole('manager')){
		return true;
	}else{
		return false;
	}
}
function permission_denied(){
	 return 'Permission denied';
}
