<?php

namespace App\Imports;

use App\Device;
use Maatwebsite\Excel\Concerns\ToModel;

class DevicesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Device([
            //
        ]);
    }
}
