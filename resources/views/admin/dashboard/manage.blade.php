@extends('layouts.app')
@section('content')
<style>
 #map {
  height: 400px;
  background-color: grey;
}   
</style>
    <div class="content-heading">
        <div>
            Dashboard
            <small data-localize="dashboard.WELCOME"></small>
        </div>
       
    </div>
    <!-- START cards box-->
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- START card-->
            
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left"><em class="icon-cloud-upload fa-3x"></em></div>
                <div class="col-8 py-3 bg-primary rounded-right">
                <a href="{{route('admin.city.manage')}}" class="dashboard-a">
                    <div class="h2 mt-0"><?= $cities?></div>
                    <div class="text-uppercase">
                        Cities
                    </div>
                </a>
                </div>
            </div>
            
        </div>
        <div class="col-xl-3 col-md-6">
            <!-- START card-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left"><em class="icon-globe fa-3x"></em></div>
                <div class="col-8 py-3 bg-purple rounded-right">
                    <a href="{{route('admin.device_history.manage')}}" class="dashboard-a">
                    <div class="h2 mt-0">
                        <?= $recent_data;?>
                    </div>
                    <div class="text-uppercase">Recent Received Data</div>
                     </a>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-12">
            <!-- START card-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-green-dark justify-content-center rounded-left"><em class="icon-bubbles fa-3x"></em></div>
                <div class="col-8 py-3 bg-green rounded-right">
                    <a href="{{route('admin.device.manage')}}" class="dashboard-a">
                    <div class="h2 mt-0"><?= $devices;?></div>
                    <div class="text-uppercase">Devices</div>
                    </a>
                </div>
            </div>
        </div>


        <div class="col-xl-3 col-md-6">
            <!-- START card-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left"><em class="fas fa-code-branch fa-2x text"></em></div>
                <div class="col-8 py-3 bg-purple rounded-right">
                    <a href="{{route('admin.school.manage')}}" class="dashboard-a">
                    <div class="h2 mt-0">
                        <?= $schools?>
                        <!-- <small>GB</small> -->
                    </div>
                    <div class="text-uppercase">Schools</div>
                    </a>
                </div>
            </div>
        </div>
        
    </div>
    <!-- END cards box-->
    <div class="row">
        <!-- START dashboard main content-->
        <div class="col-xl-9">
            <!-- START chart-->
            <div class="row">
                <div class="col-xl-12">
                    <!-- START card-->
                    <div class="card card-default card-demo" id="cardChart9">
                        <div class="card-header">
                            <a class="float-right" href="#" data-tool="card-refresh" data-toggle="tooltip" title="Refresh card"><em class="fas fa-sync"></em></a>
                            <a class="float-right" href="#" data-tool="card-collapse" data-toggle="tooltip" title="Collapse card"><em class="fa fa-minus"></em></a>
                            <div class="card-title">Statistics</div>
                        </div>
                        <div class="card-wrapper">
                            <div class="card-body">
                                <div class="chart-spline flot-chart">
                                    <div id="map"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END card-->
                </div>
            </div>
            <!-- END chart-->
            <div class="col-xl-12">
                <!-- START card tab-->
                <div class="card card-transparent" role="tabpanel">
                    <!-- Nav tabs-->
                    <ul class="nav nav-tabs nav-fill" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active bb0 float-left" href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                <em class="far fa-clock fa-fw"></em>
                               Top 10 Apps Usage
                            </a>
                             <button class="dropdown-toggle dropdown-toggle-nocaret float-right btn btn-secondary btn-sm" type="button" data-toggle="dropdown" aria-expanded="false">Select</button>
                                <div class="dropdown-menu dropdown-menu-right-forced fadeInLeft animated" role="menu" style="">
                                    <a class="dropdown-item" href="{{route('admin.dashboard.manage')}}">Today</a>
                                    <a class="dropdown-item" href="{{route('admin.dashboard.manage')}}?from=lastweek">Last Week</a>
                                    <a class="dropdown-item" href="{{route('admin.dashboard.manage')}}?from=month">Monthly</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{route('admin.dashboard.manage')}}?from=all">All time</a>
                                </div>
                            
                        </li>
                      
                       
                    </ul>
                    

                    <!-- Tab panes-->
                    <div class="tab-content p-0 bg-white">

                        <div class="tab-pane active" id="home" role="tabpanel">
                            <!-- START table responsive-->
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>App Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($j = 1)
                                        @foreach($history as $v)
                                        <tr>
                                            <td>{{$j++}}</td>
                                            <td>{{$v->app_name}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- END table responsive-->
                           <!--  <div class="card-footer text-right"><a class="btn btn-secondary btn-sm" href="#">View All Transactions</a></div> -->
                        </div>
                    </div>
                </div>
                <!-- END card tab-->
            </div>
            <div class="col-xl-6">
                <div class="card card-transparent">
                    <div data-vector-map="" data-height="450" data-scale="0" data-map-name="world_mill_en"></div>
                </div>
            </div>
           
        </div>
        <!-- END dashboard main content-->
        <!-- START dashboard sidebar-->
        <aside class="col-xl-3">
            <!-- START loader widget-->
            <div class="card card-default">
                <div class="card-body">
                    <a class="text-muted float-right" href="#"><em class="fa fa-arrow-right"></em></a>
                    <div class="text-info">Last Login</div>
                    <div class="text-center py-4">
                        <div
                            class="easypie-chart easypie-chart-lg"
                            data-easypiechart="data-easypiechart"
                            data-percent="70"
                            data-animate='{"duration": "800", "enabled": "true"}'
                            data-bar-color="#23b7e5"
                            data-track-Color="rgba(200,200,200,0.4)"
                            data-scale-Color="false"
                            data-line-width="10"
                            data-line-cap="round"
                            data-size="145"
                        >
                            <span>{{
                                dt_time_format(Auth::user()->last_login_at)[0]}}</span>

                        </div>
                    </div>
                    <div
                        class="text-center"
                        data-sparkline=""
                        data-bar-color="#23b7e5"
                        data-height="30"
                        data-bar-width="5"
                        data-bar-spacing="2"
                        data-values="5,4,8,7,8,5,4,6,5,5,9,4,6,3,4,7,5,4,7"
                    ></div>
                </div>
                <div class="card-footer">
                    <p class="text-muted">
                        <em class="fa fa-upload fa-fw"></em>
                        <span>Time</span>
                        <span class="text-dark"> <span>{{
                                dt_time_format(Auth::user()->last_login_at)[1]}}</span></span>
                    </p>
                </div>

            </div>
            <!-- END loader widget-->
            <!-- START messages and activity-->
             <div class="card card-default">
                <div class="card-body">
                    <a class="text-muted float-right" href="#">
                        <em class="fa fa-arrow-right"></em> </a>
                     <a href="{{route('admin.user.manage')}}">
                    <div class="text-info">Total Users</div>
                    <div class="text-center py-4">
                        <div
                            class="easypie-chart easypie-chart-lg"
                            data-easypiechart="data-easypiechart"
                            data-percent="70"
                            data-animate='{"duration": "800", "enabled": "true"}'
                            data-bar-color="#23b7e5"
                            data-track-Color="rgba(200,200,200,0.4)"
                            data-scale-Color="false"
                            data-line-width="10"
                            data-line-cap="round"
                            data-size="145"
                        >
                            <span>{{$users}}</span>

                        </div>
                    </div>
                    <div
                        class="text-center"
                        data-sparkline=""
                        data-bar-color="#23b7e5"
                        data-height="30"
                        data-bar-width="5"
                        data-bar-spacing="2"
                        data-values="5,4,8,7,8,5,4,6,5,5,9,4,6,3,4,7,5,4,7"
                    ></div>
                </a>
                   
                </div>
                <div class="card-footer">
                    <p class="text-muted">
                        <em class="fa fa-upload fa-fw"></em>
                        <span></span>
                        <span class="text-dark"> <span></span></span>
                    </p>
                </div>

            </div>
            <!-- END messages and activity-->
        </aside>
        <!-- END dashboard sidebar-->
    </div>
@endsection
@section('scripts')
    <script src="{{ mix('/js/sparkline.js') }}"></script>
    <script src="{{ mix('/js/easypiechart.js') }}"></script>
    <script src="{{ mix('/js/flot.js') }}"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRiY3ROrigvgVNs1tX7doF97kfFosQ8_o&callback=initMap">
</script>
<script>

function initMap() {
  var locations = <?php echo $location; ?>;
  var myLatLng = { lat: 25.1935586, lng: 66.8745918 };
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 1,
    center: myLatLng,
  });

  for (count = 0; count < locations.length; count++) {
    var lt = { lat: locations[count][1], lng: locations[count][2] };
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[count][1], locations[count][2]),
      map: map,
      title: locations[count][0]
    });
  }

}
</script>
@endsection
