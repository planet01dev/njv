@extends('layouts.app')
@section('content')

    <div class="content-heading">
        <div>
            Customer {{@$title}}
            <!-- <small>Standard and custom elements for any form</small> -->
        </div>
    </div>
   
    <!-- START row-->
    <div class="row">
        <div class="col-md-12">
            <!-- START card-->
            <div class="card card-default">
                <div class="card-header">Customer Form</div>
                <div class="card-body">
                    <form method="POST" class="form  ajaxForm" action="{{route('admin.user.post')}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Name</label>
                                <input class="form-control" value="{{@$data->name}}" name="name" type="text" required placeholder="Name" />
                            </div>
                            <div class="col-md-6">
                                <label>Email</label>
                                <input class="form-control"  {{ (is_admin())? '' : 'readonly' }} value="{{@$data->email}}" name="email" required type="email" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Password</label>
                                <input class="form-control"  name="password" type="password" placeholder="Password" />
                            </div>
                            <div class="col-md-6">
                                <label>Confirm Password</label>
                                <input class="form-control"  name="password_confirmation" type="password" placeholder="Confirm Password" />
                            </div>
                        </div>
                        @if(is_admin())
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>User Type</label>
                                <select name="user_type" class="form-control">
                                    @foreach($role as $role)
                                        <option {{ (isset($data->user_type) && $data->user_type == $role->id) ? 'selected' : '' }} value="{{$role->id}}">{{@ucfirst($role->name)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @else
                          <input type="hidden" name="user_type" value="{{@$data->user_type}}"/>
                        @endif
                       <!--  <div class="form-group row">
                            <div class="col-md-6">
                                <label>Country</label>
                                <input class="form-control" value="{{@$data->country}}" name="country" required type="text" placeholder="Country" />
                            </div>
                            <div class="col-md-6">
                                <label>City</label>
                                <input class="form-control" value="{{@$data->city}}" name="city"  required type="text" placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pin Code</label>
                                <input class="form-control" value="{{@$data->pin_code}}" name="pin_code" required type="text" placeholder="Pin Code" />
                            </div>
                            <div class="col-md-6">
                                <label>Address</label>
                                <textarea class="form-control" name="address"  required>{!! @$data->address !!}</textarea>
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                             <input type="hidden" name="id" value="{{@$data->id}}"/>
                             <button class="btn btn-primary btn-lg ajaxFormSubmit" type="button">Submit</button>
                             <a href="{{route('admin.user.manage')}}">
                                <button class="btn btn-info btn-lg" type="button">Back</button>
                            </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END card-->
        </div>
       
    </div>
    <!-- END row-->
   
@endsection
@section('styles')@endsection
@section('scripts')
<script>
   $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        name:{
          required: true
        },
        email:{
          required: true
        },
        user_type:{
          required: true
        },
       /* city:{
          required: true
        },
        country:{
          required: true
        },
        pin_code:{
          required: true
        },
        address:{
          required: true
        },*/
      }, 
      messages: {
        category_name: "This field is required.",
        category_is_active: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
  });
</script>

@endsection
