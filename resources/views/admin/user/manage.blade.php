@extends('layouts.app')
@section('content')
    <div class="content-heading">
        <div>
            Customer Details
           <!--  <small>Tables, one step forward.</small> -->
        </div>
    </div>
    <div class="container-fluid">
        <!-- DATATABLE DEMO 1-->
        <div class="card">
            <div class="card-header">
                <div class="card-title">Customer Details</div>
                @if(is_admin())
                <div class="text-sm text-right">
                    <a href="{{route('admin.user.add')}}">
                        <button class="mb-1 btn btn-primary" type="button">Add</button>
                    </a>
                </div>
                @endif
            </div>
            <div class="card-body">
                <table class="table table-striped my-4 w-100" id="datatable1">
                    <thead>
                        <tr>
                            <th data-priority="1">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th class="sort-alpha" data-priority="2">User Type</th>
                            <th class="sort-alpha" data-priority="2">Date</th>
                            <th class="sort-alpha" data-priority="2">Time</th>
                            @if(is_admin())
                                <th class="sort-alpha" data-priority="2">Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($data as $dt)
                        <tr class="gradeX">
                            <td>{{$i++}}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{!! $dt->email !!}</td>
                            <td>{{@$dt->role->name}}</td>
                            <td>{{dt_time_format($dt->updated_at)[0]}}</td>
                            <td>{{dt_time_format($dt->updated_at)[1]}}</td>
                            <td>
                               <!--  <em class="fa-1x mr-2 far fa-eye"></em>  -->
                               @if(is_admin())
                                <a href="{{route('admin.user.edit', $dt->id)}}">
                                    <em class="fa-1x mr-2 fas fa-pencil-alt"></em> 
                                </a>
                                <!-- <a href="#">
                                <em onclick="item_delete({{$dt->id}}, '{{route('admin.user.delete', $dt->id)}}')" class="fa-1x mr-2 fas fa-trash-alt"></em> 
                                </a> -->
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
      
    </div>
@endsection
@section('styles')@endsection
@section('scripts')
    <script src="{{ asset('/public/js/datatable.js') }}"></script>
@endsection
