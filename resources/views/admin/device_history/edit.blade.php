@extends('layouts.app')
@section('content')

    <div class="content-heading">
        <div>
            Device History {{@$title}}
            <!-- <small>Standard and custom elements for any form</small> -->
        </div>
    </div>
   
    <!-- START row-->
    <div class="row">
        <div class="col-md-12">
            <!-- START card-->
            <div class="card card-default">
                <div class="card-header">Device History Form</div>
                <div class="card-body">
                    <form method="POST" class="form ajaxForm validate" action="{{route('admin.device_history.post')}}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Device Number</label>
                                <div class="input-with-icon right controls">
                                    
                                    <select name="device_id" class="form-control select2">
                                    @foreach($devices as $v)
                                    <option <?= (@$data->device_id == $v->id)?'selected':''; ?> value="{{$v->id}}">{{$v->device_number}}</option>
                                    @endforeach
                                  </select>
                                  </div>
                            </div>
                            <div class="col-md-6">
                                <label>MAC Address</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->mac_address}}" name="mac_address" required type="text" placeholder="MAC Address" />
                                  </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Latitude</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->latitude}}" <?= (@$title == 'Edit')?'disabled':''?>  name="latitude" type="text" placeholder="Latitude" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Longitude</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->longitude}}" <?= (@$title == 'Edit')?'disabled':''?>  name="longitude" type="text" placeholder="Longitude" />
                                  </div>
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>App Name</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->app_name}}" <?= (@$title == 'Edit')?'disabled':''?>  name="app_name" type="text" placeholder="App Name" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Minutes</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->minutes}}" <?= (@$title == 'Edit')?'disabled':''?>  name="minutes" type="text" placeholder="Minutes" />
                                  </div>
                                
                            </div>
                            <?php if(@$title == 'Edit'){ ?>
                            <div class="col-md-6">
                                <label>Last Successful Data Upload Time</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->last_successful_data_upload_time}}" <?= (@$title == 'Edit')?'disabled':''?>  name="last_successful_data_upload_time" type="text" placeholder="Last Successful Data Upload Time" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Contacts Permission</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="<?= ($data->contacts_permission == 1)?'Yes':'No' ?>" <?= (@$title == 'Edit')?'disabled':''?>  name="contacts_permission" type="text" placeholder="Contacts Permission" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Location Permission</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="<?= ($data->location_permission== 1)?'Yes':'No' ?>" <?= (@$title == 'Edit')?'disabled':''?>  name="location_permission" type="text" placeholder="Location Permission" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Device Admin Permission</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="<?= ($data->device_admin_permission== 1)?'Yes':'No' ?>" <?= (@$title == 'Edit')?'disabled':''?>  name="device_admin_permission" type="text" placeholder="Device Admin Permission" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Usage Status Permission</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="<?= ($data->usage_status_permission== 1)?'Yes':'No' ?>" <?= (@$title == 'Edit')?'disabled':''?>  name="usage_status_permission" type="text" placeholder="usage_status_permission" />
                                  </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label>Primary Email Of Device</label>
                                <div class="input-with-icon right controls">
                                    <input class="form-control" value="{{@$data->primary_email_of_device}}" <?= (@$title == 'Edit')?'disabled':''?>  name="primary_email_of_device" type="text" placeholder="Primary Email Of Device" />
                                  </div>
                                
                            </div>
                            <?php } ?>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                             <input type="hidden" name="id" value="{{@$data->id}}"/>
                             <button class="btn btn-primary btn-lg ajaxFormSubmit" type="button">Submit</button>
                             <a href="{{route('admin.device.manage')}}">
                                <button class="btn btn-info btn-lg" type="button">Back</button>
                            </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END card-->
        </div>
       
    </div>
    <!-- END row-->
   
@endsection
@section('styles')

@endsection
@section('scripts')
<script>
   $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        device_id:{
          required: true
        },
        mac_address:{
          required: true
        },
        latitude:{
          required: true
        },
        longitude:{
          required: true
        },
        app_name:{
          required: true
        },
        minutes:{
          required: true
        },
        
       
      }, 
      messages: {
        
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");

      },
      errorPlacement: function(label, element) { // render error placement for each input type   
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }
        
      });
  });
</script>

@endsection
