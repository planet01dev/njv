@extends('layouts.app')
@section('content')
    <div class="content-heading">
        <div>
            Device History Details
           <!--  <small>Tables, one step forward.</small> -->
        </div>
    </div>
    <div class="container-fluid">
        <!-- DATATABLE DEMO 1-->
        <div class="card pagnation-body">
            <div class="card-header">
                <div class="card-title">Device History Details</div>
                @if(is_admin())
                <div class="text-sm text-right">
                    <a href="{{route('admin.device_history.add')}}">
                        <button class="mb-1 btn btn-primary" type="button">Add</button>
                    </a>
                </div>
                @endif
            </div>
            <div class="card-body ">
                <table class="table table-striped my-4 w-100 " id="">
                    <thead>
                        <tr>
                            <th data-priority="1">#</th>
                            <th>Device No.</th>
                            <th>MAC Address</th>
                            <th class="sort-alpha" data-priority="2">latitude</th>
                            <th class="sort-alpha" data-priority="2">longitude</th>
                            <th class="sort-alpha" data-priority="2">app_name</th>
                            <th class="sort-alpha" data-priority="2">minutes</th>
                            <th class="sort-alpha" data-priority="2">Upload Time</th>
                            <th class="sort-alpha" data-priority="2">Contacts Permission</th>
                            <th class="sort-alpha" data-priority="2">Location Permission</th>
                            <th class="sort-alpha" data-priority="2">Device Admin Permission</th>
                            <th class="sort-alpha" data-priority="2">Usage Status Permission</th>
                            <th class="sort-alpha" data-priority="2">Primary Email Of Device</th>
                            @if(is_admin() || is_manager())
                                <th class="sort-alpha" data-priority="2">Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                        @foreach($data as $dt)
                        <tr class="gradeX">
                            <td>{{$i++}}</td>
                            <td>{{ @$dt->device['device_number'] }}</td>
                            <td>{{ $dt->mac_address }}</td>
                            <td>{{ $dt->latitude }}</td>
                            <td>{{$dt->longitude}}</td>
                            <td>{{$dt->app_name}}</td>
                            <td>{{($dt->minutes / 1000)}}</td>
                            <td>{{$dt->last_successful_data_upload_time}}</td>
                            <td><?= ($dt->contacts_permission == 1)?'Yes':'No' ?></td>
                            <td><?= ($dt->location_permission== 1)?'Yes':'No' ?></td>
                            <td><?= ($dt->device_admin_permission== 1)?'Yes':'No' ?></td>
                            <td><?= ($dt->usage_status_permission== 1)?'Yes':'No' ?></td>
                            <td>{{$dt->primary_email_of_device}}</td>
                            <td>
                               <!--  <em class="fa-1x mr-2 far fa-eye"></em>  -->
                               @if(is_admin() || is_manager())
                                <a href="{{route('admin.device_history.edit', $dt->id)}}">
                                    <em class="fa-1x mr-2 fas fa-pencil-alt"></em> 
                                </a>
                                <!-- <a href="#">
                                <em onclick="item_delete({{$dt->id}}, '{{route('admin.device_history.delete', $dt->id)}}')" class="fa-1x mr-2 fas fa-trash-alt"></em> 
                                </a> -->
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
<?php if (isset($data)) { ?>
  {!! $data->render() !!}
<?php } ?>
        </div>
      
    </div>
@endsection
@section('styles')@endsection
@section('scripts')
    <script src="{{ asset('/public/js/datatable.js') }}"></script>
@endsection
