@extends('layouts.app')
@section('content')

<div class="content-heading">
  <div>
    {{@$title}}
    <!-- <small>Standard and custom elements for any form</small> -->
  </div>
</div>

<!-- START row-->
<div class="row">
  <div class="col-md-12">
    <!-- START card-->
    <div class="card card-default">
      <div class="card-header">{{@$title}} Form</div>
      <div class="card-body">
        <form method="POST" class="form ajaxForm validate" action="{{route('admin.city.post')}}">
          @csrf
          <div class="form-group row">
            <div class="col-md-12">
              <label>Title</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->title}}" name="title" type="text" required placeholder="Title" />
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="form-group row">
            <div class="col-md-12 text-center">
              <input type="hidden" name="id" value="{{@$data->id}}" />
              <button class="btn btn-primary btn-lg ajaxFormSubmit" type="button">Submit</button>
              <a href="{{route('admin.city.manage')}}">
                <button class="btn btn-info btn-lg" type="button">Back</button>
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- END card-->
  </div>

</div>
<!-- END row-->

@endsection
@section('styles')

@endsection
@section('scripts')
<script>
  $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        title: {
          required: true
        }

      },
      messages: {
        device_number: "This field is required.",
        name: "This field is required.",
        father_name: "This field is required.",
        phone: "This field is required.",
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");

      },
      errorPlacement: function(label, element) { // render error placement for each input type   
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }

    });
  });



</script>

@endsection