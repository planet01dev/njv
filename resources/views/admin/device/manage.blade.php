@extends('layouts.app')
@section('content')
    <div class="content-heading">
        <div>
        Device Details
           <!--  <small>Tables, one step forward.</small> -->
        </div>
    </div>
    <div class="container-fluid">
        <!-- DATATABLE DEMO 1-->
        <div class="card pagnation-body">
            <div class="card-header">
                <div class="card-title">Device Details</div>
                @if(is_admin())
                <div class="row">
                    <div class="col-md-6">
                      <form method="GET" action="{{route('admin.device.manage')}}">
                        <div class="row">
                            <div class="text-sm col-md-5">
                                <select name="city" class="form-control">
                                    <option {{(!isset($filter) && !isset($filter['country']))? 'selected' : ''}} value="" >-- Select Country --</option>
                                    @foreach($cities as $v)
                                        <option {{(isset($filter) && isset($filter['country']) && $filter['country'] == $v->id)? 'selected' : ''}} value="{{$v->id}}">{{$v->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-5">
                                 <select name="school" class="form-control">
                                    <option {{(!isset($filter) && !isset($filter['school']))? 'selected' : ''}} value="" >-- Select School --</option>
                                    @foreach($schools as $v)
                                        <option {{(isset($filter) && isset($filter['school']) && $filter['school'] == $v->id)? 'selected' : ''}}  value="{{$v->id}}">{{$v->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('admin.device.add')}}">
                                    <button class="mb-1 btn btn-primary" type="submit">Search</button>
                                </a>
                            </div>
                        </div>
                      </form>
                    </div>
                    <div class="col-md-6">
                        <div class="text-sm text-right">
                            <a href="{{route('admin.device.export')}}">
                                <button class="mb-1 btn btn-primary" type="button">Export CSV</button>
                            </a>
                            <a href="{{route('admin.device.import')}}">
                                <button class="mb-1 btn btn-primary" type="button">Import CSV</button>
                            </a>
                            <a href="{{route('admin.device.add')}}">
                                <button class="mb-1 btn btn-primary" type="button">Add</button>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="card-body">
                <table class="table table-striped my-4 w-100" id="">
                    <thead>
                        <tr>
                            <th data-priority="1">#</th>
                            <th>Device No.</th>
                            <th>Mac Address</th>
                            <th>Issued To</th>
                            <th class="sort-alpha" data-priority="2">School</th>
                            <th class="sort-alpha" data-priority="2">City</th>
                            <th class="sort-alpha" data-priority="2">Last Update</th>
                            @if(is_admin() || is_manager())
                                <th class="sort-alpha" data-priority="2">Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                        @foreach($data as $dt)
                        <tr class="gradeX">
                            <td>{{$i++}}</td>
                            <td>{{ $dt->device_number }}</td>
                            <td>{{ $dt->device_mac_address }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ @$dt->school['title'] }}</td>
                            <td>{{@$dt->city['title']}}</td>
                            <td>{{dt_time_format($dt->updated_at)[1]}}</td>
                            <td>
                               <!--  <em class="fa-1x mr-2 far fa-eye"></em>  -->
                               @if(is_admin() || is_manager())
                                <a href="{{route('admin.device.edit', $dt->id)}}">
                                    <em class="fa-1x mr-2 fas fa-pencil-alt"></em> 
                                </a>
                                <a href="{{route('admin.device.detail', $dt->id )}}"  title="View Detail" >
                                    <em class="fa-1x mr-2 far fa-eye"></em>
                                </a>
                               <!--  <a href="#">
                                <em onclick="item_delete({{$dt->id}}, '{{route('admin.device.delete', $dt->id)}}')" class="fa-1x mr-2 fas fa-trash-alt"></em> 
                                </a> -->
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
<?php if (isset($data)) { ?>
  {!! $data->render() !!}
<?php } ?>
        </div>
      
    </div>
@endsection
@section('styles')@endsection
@section('scripts')
    <script src="{{ asset('/public/js/datatable.js') }}"></script>
@endsection
