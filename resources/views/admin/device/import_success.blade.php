@extends('layouts.app')

@section('content')
<div class="content-heading">
    <div>
        Device Import
        <!--  <small>Tables, one step forward.</small> -->
    </div>
</div>
<div class="container-fluid">
    <!-- DATATABLE DEMO 1-->
    <div class="card">
        <div class="card-header">
            <div class="card-title">Device Import</div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">CSV Import</div>

                        <div class="panel-body">
                            Data imported successfully.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection