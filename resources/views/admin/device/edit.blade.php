@extends('layouts.app')
@section('content')

<div class="content-heading">
  <div>
    Device {{@$title}}
    <!-- <small>Standard and custom elements for any form</small> -->
  </div>
</div>

<!-- START row-->
<div class="row">
  <div class="col-md-12">
    <!-- START card-->
    <div class="card card-default">
      <div class="card-header">Device Form</div>
      <div class="card-body">
        <form method="POST" class="form ajaxForm validate" action="{{route('admin.device.post')}}">
          @csrf

          <div class="form-group row">
            <div class="col-md-6">
              <label>Device Number</label>
              <div class="input-with-icon right controls">
                <input class="form-control" {{(Request::route()->getName() == 'admin.device.add')? '': 'readonly'}} value="{{@$data->device_number}}" name="device_number" type="text" required placeholder="Device Number" />
              </div>
            </div>
            <div class="col-md-6">
              <label>Name</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->name}}" name="name" required type="text" placeholder="Name" />
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <label>Father Name</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->father_name}}" name="father_name" type="text" placeholder="Father Name" />
              </div>

            </div>
            <div class="col-md-6">
              <label>Phone</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->phone}}" name="phone" type="text" placeholder="Phone No" />
              </div>

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <label>City</label>
              <div class="input-with-icon right controls">
                <select  class="form-control" name="city_id">
                  @foreach($cities as $v)
                    <option {{(isset($data) && $data->city_id = $v->id)? 'selected' : ''}} value="{{$v->id}}">{{$v->title}}</option>
                  @endforeach
                </select>
              </div>

            </div>
            <div class="col-md-6">
              <label>Province</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->province}}" name="province" type="text" placeholder="Province" />
              </div>

            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label>Country</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->country}}" name="country" type="text" placeholder="Country" />
              </div>

            </div>
            <div class="col-md-6">
              <label>School</label>
              <div class="input-with-icon right controls">
               <select  class="form-control" name="school_id">
                  @foreach($schools as $v)
                    <option {{(isset($data) && $data->school_id = $v->id)? 'selected' : ''}} value="{{$v->id}}">{{$v->title}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label>Student Class</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->student_class}}" name="student_class" type="text" placeholder="Student Class" />
              </div>

            </div>
            <div class="col-md-6">
              <label>Student Group</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->student_group}}" name="student_group" type="text" placeholder="Student Group" />
              </div>

            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label>Section</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->section}}" name="section" type="text" placeholder="Section" />
              </div>

            </div>
            <div class="col-md-6">
              <label>Board</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->board}}" name="board" type="text" placeholder="Board" />
              </div>

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <label>Mac Address</label>
              <div class="input-with-icon right controls">
                <input class="form-control" value="{{@$data->device_mac_address}}" name="device_mac_address" type="text" placeholder="Mac Address" />
              </div>

            </div>
          </div>

          <div class="clearfix"></div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-12">
                <div class="responsive-scroll" style="width: 100%;">
                  <table class="table table-bordered" style="display:block; overflow-x:scroll">
                    <thead>
                      <tr>
                        <th class="text-center" style="width:60px;">Add/Remove Row</th>
                        <th class="text-center" style="min-width:200px;">MAC Address</th>
                        <th class="text-center" style="min-width:200px;">Latitude</th>
                        <th class="text-center" style="min-width:200px;">Longitude</th>
                        <th class="text-center" style="min-width:200px;">App Name</th>
                        <th class="text-center" style="min-width:200px;">Minutes</th>
                      </tr>
                    </thead>
                    <tr class="txtMult">
                      <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                      <td colspan="5"></td>
                    </tr>

                    <tbody id="customFields">

                      @php($k = 1)
                      @if(!empty(@$data->id) && @$data->id != null)
                      @foreach(@$detail_data as $v)
                      <tr class="txtMult">
                        <td class="text-center" style="width:40px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>

                        <td>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type='text' class='txtboxToFilter mac_address' value="{{@$v->mac_address}}" required style='width:100%' id='mac_address{{ $k }}' data-optional='0' name='mac_address[]' />
                          </div>
                        </td>

                        <td>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type='text' class='txtboxToFilter latitude' value="{{@$v->latitude}}" required style='width:100%' id='latitude{{ $k }}' data-optional='0' name='latitude[]' />
                          </div>
                        </td>

                        <td>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type='text' class='txtboxToFilter longitude' value="{{@$v->longitude}}" required style='width:100%' id='longitude{{ $k }}' data-optional='0' name='longitude[]' />
                          </div>
                        </td>

                        <td>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type='text' class='txtboxToFilter app_name' value="{{@$v->app_name}}" required style='width:100%' id='app_name{{ $k }}' data-optional='0' name='app_name[]' />
                          </div>
                        </td>

                        <td>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type='text' class='txtboxToFilter minutes' value="{{@$v->minutes}}" required style='width:100%' id='minutes{{ $k }}' data-optional='0' name='minutes[]' />
                          </div>
                        </td>


                      </tr>
                      @php($k ++)
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="form-group row">
            <div class="col-md-12 text-center">
              <input type="hidden" name="id" value="{{@$data->id}}" />
              <button class="btn btn-primary btn-lg ajaxFormSubmit" type="button">Submit</button>
              <a href="{{route('admin.device.manage')}}">
                <button class="btn btn-info btn-lg" type="button">Back</button>
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- END card-->
  </div>

</div>
<!-- END row-->

@endsection
@section('styles')

@endsection
@section('scripts')
<script>
  $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        device_number: {
          required: true
        },
        name: {
          required: true
        },
        father_name: {
          required: true
        },
        phone: {
          required: true
        },
        city: {
          required: true
        },
        province: {
          required: true
        },
        country: {
          required: true
        },
        school: {
          required: true
        },
        student_class: {
          required: true
        },
        student_group: {
          required: true
        },
        section: {
          required: true
        },
        board: {
          required: true
        },

      },
      messages: {
        device_number: "This field is required.",
        name: "This field is required.",
        father_name: "This field is required.",
        phone: "This field is required.",
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");

      },
      errorPlacement: function(label, element) { // render error placement for each input type   
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }

    });
  });

  var x = 0;
  var add_button = $(".addCF");
  $(add_button).click(function(e) {
    
    x++;
    e.preventDefault();
    $('form.validate').validate();
    var temp = '<tr class="txtMult">';
    temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';

       
    temp += '<td>';
    temp += "<div class='input-with-icon right controls'>";
    temp += "<i class=''></i>";
    temp += "<input type='text'  class='txtboxToFilter mac_address'  required style='width:100%' id='mac_address" + x + "' data-optional='0' name='mac_address[]' />";
    temp += "</div>";
    temp += '</td>';

    temp += '<td>';
    temp += "<div class='input-with-icon right controls'>";
    temp += "<i class=''></i>";
    temp += "<input type='text'  class='txtboxToFilter latitude'  required style='width:100%' id='latitude" + x + "' data-optional='0' name='latitude[]' />";
    temp += "</div>";
    temp += '</td>';

    temp += '<td>';
    temp += "<div class='input-with-icon right controls'>";
    temp += "<i class=''></i>";
    temp += "<input type='text'  class='txtboxToFilter longitude'  required style='width:100%' id='longitude" + x + "' data-optional='0' name='longitude[]' />";
    temp += "</div>";
    temp += '</td>';

    temp += '<td>';
    temp += "<div class='input-with-icon right controls'>";
    temp += "<i class=''></i>";
    temp += "<input type='text'  class='txtboxToFilter app_name'  required style='width:100%' id='app_name" + x + "' data-optional='0' name='app_name[]' />";
    temp += "</div>";
    temp += '</td>';

    temp += '<td>';
    temp += "<div class='input-with-icon right controls'>";
    temp += "<i class=''></i>";
    temp += "<input type='text'  class='txtboxToFilter minutes'  required style='width:100%' id='minutes" + x + "' data-optional='0' name='minutes[]' />";
    temp += "</div>";
    temp += '</td>';


    
    temp += '</tr>';

    $("#customFields").append(temp);

  });

  $("#customFields").on('click', '.remCF', function() {
    $(this).parent().parent().remove();
  });

</script>

@endsection