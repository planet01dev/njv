@extends('layouts.app')
@section('content')


<div class="content-heading">
  <div>
    {{@$title}}
    <!-- <small>Standard and custom elements for any form</small> -->
  </div>
</div>

<!-- START row-->
<div class="row">
  <div class="col-md-12">
    <!-- START card-->
    <div class="card card-default">
      <div class="card-header">Device Detail &nbsp; 
        <span class="text-right">
          @if(is_admin() || is_manager())
                <a href="{{route('admin.device.edit', $data->id)}}" title="Edit">
                    <em class="fa-1x mr-2 fas fa-pencil-alt"></em> 
                </a>
                <a href="{{route('admin.device.single.report', $data->id)}}">
                    <button class="mb-1 btn btn-primary" type="button">Export CSV</button>
                </a>
                
          @endif
        </span>
      </div>
      <div class="card-body">
                  <table class="table table-bordered table-striped">
                      <!--<tr>-->
                      <!--  <td>Perfoma Invoice No</td>-->
                      <!--  <td>{{ $data->perfoma_invoice_no }}</td>-->
                      <!--</tr>-->
                      <tr>
                        <td>Device Number</td>
                        <td>{{ $data->device_number }}</td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td>{{ $data->name}}</td>
                      </tr>
                      <tr>
                        <td>Father Name</td>
                        <td><div>{!! $data->father_name !!}</div></td>
                      </tr>

                      <tr>
                        <td>Phone</td>
                        <td><div>{!! $data->phone !!}</div></td>
                      </tr>
                      <tr>
                        <td>City</td>
                        <td><div>{!! @$data->city->title !!}</div></td>
                      </tr>
                      <tr>
                        <td>Province</td>
                        <td><div>{!! $data->province !!}</div></td>
                      </tr>
                      <tr>
                        <td>Country</td>
                        <td><div>{!! $data->country !!}</div></td>
                      </tr>
                      <tr>
                        <td>School</td>
                        <td><div>{!! @$data->school->title !!}</div></td>
                      </tr>
                      <tr>
                        <td>Student Class</td>
                        <td><div>{!! $data->student_class !!}</div></td>
                      </tr>
                      <tr>
                        <td>Student Group</td>
                        <td><div>{!! $data->student_group !!}</div></td>
                      </tr>
                      <tr>
                        <td>Section</td>
                        <td><div>{!! $data->section !!}</div></td>
                      </tr>
                      <tr>
                        <td>Board</td>
                        <td><div>{!! $data->board !!}</div></td>
                      </tr>
                      

                      
                  </table>
                  
                  <br>
                  
                  <div class="form-group">
                          <div class="row" >
                              <div class="col-md-12">
                                <div class="responsive-scroll" style="width: 100%;">
                                  <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                      <th class="text-center" style="width:60px;">S.No</th>
                                      <th class="text-center" style="min-width:200px;">Date</th>
                                      <th class="text-center" style="min-width:200px;">MAC Address</th>
                                      <th class="text-center" style="min-width:200px;">Latitude</th>
                                      <th class="text-center" style="min-width:200px;">Longitude</th>
                                      </tr>
                                    </thead>
                                    <tbody id="customFields">
                                      
                                         @php($i=1)
                                         @foreach(@$data->detail_data as $k => $v)

                                          <tr class="txtMult">
                                              <td class="text-center" style="width:60px;">{{$i++}}</td>
                                              <td>
                                                  <a href="{{route('admin.device_history.manage')}}?device_id={{@$v[0]->device_id}}&date={{$k}}">{{dt_format($k)}}</a>
                                              </td>
                                              <td>
                                                  {{@$v[0]->mac_address}}
                                              </td>
                                              
                                              <td>
                                                {{ @$v[0]->latitude }}
                                              </td>
                                              <td>
                                                {{@$v[0]->longitude}}
                                              </td>
                                              
                                          </tr>
                                          @endforeach
                                    </tbody>
                                  </table>
                                  </div>
    </div>
    <!-- END card-->
  </div>

</div>
  
@endsection
@section('footer')

@endsection