@extends('layouts.app')

@section('content')
<div class="content-heading">
    <div>
        Device Import
        <!--  <small>Tables, one step forward.</small> -->
    </div>
</div>
<div class="container-fluid">
    <!-- DATATABLE DEMO 1-->
    <div class="card">
        <div class="card-header">
            <div class="card-title">Device Import</div>
            @if(is_admin())
            <div class="text-sm text-right">

                <a href="{{route('admin.device.add')}}">
                    <button class="mb-1 btn btn-primary" type="button">Add</button>
                </a>
            </div>
            @endif
        </div>
        <div class="card-body">
            <form class="form-horizontal" method="POST" action="{{ route('admin.device.import_process') }}">
                {{ csrf_field() }}

                <input type="hidden" name="csv_data" value="{{ $csv_data_file['csv_data'] }}">
                <table class="table">

                    @if (isset($csv_header_fields))
                    <tr>
                        @foreach ($csv_header_fields[0] as $csv_header_field)
                        <th>{{ $csv_header_field }}</th>
                        @endforeach
                    </tr>
                    @endif
                    @foreach ($csv_data[0] as $row)

                    <tr>
                        @foreach ($row as $key => $value)

                        <td>{{ $value }}</td>
                        @endforeach

                    </tr>

                    @endforeach


                </table>

                <button type="submit" class="btn btn-primary">
                    Import Data
                </button>
            </form>
        </div>
    </div>

</div>
@endsection