@extends('layouts.app')

@section('content')
<div class="content-heading">
    <div>
        Device Import
        <!--  <small>Tables, one step forward.</small> -->
    </div>
</div>
<div class="container-fluid">
    <!-- DATATABLE DEMO 1-->
    <div class="card">
        <div class="card-header">
            <div class="card-title">Device Import</div>

        </div>
        <div class="card-body">
            <form class="form-horizontal" method="POST" action="{{ route('admin.device.import_parse') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                    <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                    <div class="col-md-6">
                        <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                        @if ($errors->has('csv_file'))
                        <span class="help-block">
                            <strong>{{ $errors->first('csv_file') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" onclick="return false;" name="header" checked> File contains header row?
                            </label>
                        </div>
                    </div>
                </div>
                @if($errors->any())
                    {!! implode('', $errors->all('<div class="error">:message</div>')) !!}
                @endif
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary ">
                            Parse CSV
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection