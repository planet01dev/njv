
                  <table class="table table-bordered table-striped">

                      <tr>
                        <td>Device Number</td>
                        <td>{{ $data->device_number }}</td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td>{{ $data->name}}</td>
                      </tr>
                      <tr>
                        <td>Father Name</td>
                        <td><div>{!! $data->father_name !!}</div></td>
                      </tr>

                      <tr>
                        <td>Phone</td>
                        <td><div>{!! $data->phone !!}</div></td>
                      </tr>
                      <tr>
                        <td>City</td>
                        <td><div>{!! @$data->city->title !!}</div></td>
                      </tr>
                      <tr>
                        <td>Province</td>
                        <td><div>{!! $data->province !!}</div></td>
                      </tr>
                      <tr>
                        <td>Country</td>
                        <td><div>{!! $data->country !!}</div></td>
                      </tr>
                      <tr>
                        <td>School</td>
                        <td><div>{!! @$data->school->title !!}</div></td>
                      </tr>
                      <tr>
                        <td>Student Class</td>
                        <td><div>{!! $data->student_class !!}</div></td>
                      </tr>
                      <tr>
                        <td>Student Group</td>
                        <td><div>{!! $data->student_group !!}</div></td>
                      </tr>
                      <tr>
                        <td>Section</td>
                        <td><div>{!! $data->section !!}</div></td>
                      </tr>
                      <tr>
                        <td>Board</td>
                        <td><div>{!! $data->board !!}</div></td>
                      </tr>
                      

                      
                  </table>
                  
                  <br>
                  

                <table class="table table-bordered">
                  <thead>
                    <tr>
                    <th class="text-center" style="width:60px;">S.No</th>
                    <th class="text-center" style="min-width:200px;">MAC Address</th>
                    <th class="text-center" style="min-width:200px;">Latitude</th>
                    <th class="text-center" style="min-width:200px;">Longitude</th>
                    <th class="text-center" style="min-width:200px;">App Name</th>
                    <th class="text-center" style="min-width:200px;">Minutes</th>
                    </tr>
                  </thead>
                  <tbody id="customFields">
                    
                       @php($i=1)
                       @foreach(@$data->device_history as $v)

                       
                        <tr class="txtMult">
                            <td class="text-center" style="width:60px;">{{$i++}}</td>
                            <td>
                                {{$v->mac_address}}
                            </td>
                            
                            <td>
                              <?= $v->latitude; ?>
                            </td>
                            <td>
                              {{$v->longitude}}
                            </td>
                            <td>
                                {{@$v->app_name}} 
                            </td>
                            
                            <td>
                                {{@$v->minutes}} 
                            </td>
                            
                        </tr>
                        @endforeach
                  </tbody>
                </table>
                                 
  
