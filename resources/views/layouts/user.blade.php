<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Bootstrap Admin App" />
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="icon" type="image/x-icon" href="favicon.ico" />

        <title>{{env('APP_NAME')}}-Login</title>

        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/vendor.css') }}" />
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/bootstrap.css') }}" data-rtl="{{ asset('/public/css/bootstrap-rtl.css') }}" id="bscss" />
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/app.css') }}" data-rtl="{{ asset('/public/css/app-rtl.css') }}" id="maincss" />

        @yield('styles')
    </head>

    <body>
        <div class="wrapper">
            @yield('content')
        </div>
        @yield('body-area')
        <!-- =============== VENDOR SCRIPTS ===============-->
        <script src="{{ asset('/public/js/manifest.js') }}"></script>
        <script src="{{ asset('/public/js/vendor.js') }}"></script>
        <!-- =============== APP SCRIPTS ===============-->
        <script src="{{ asset('/public/js/app.js') }}"></script>
        <!-- =============== CUSTOM PAGE SCRIPTS ===============-->
        @yield('scripts')
    </body>
</html>
