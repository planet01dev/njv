<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Bootstrap Admin App" />
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />

        <title>{{(isset($title) && $title) ? env('APP_NAME').'-'.$title : env('APP_NAME')}}</title>

        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/vendor.css') }}" />
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/bootstrap.css') }}" data-rtl="{{ asset('/public/css/bootstrap-rtl.css') }}" id="bscss" />
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="{{ asset('/public/css/app.css') }}" data-rtl="{{ asset('/public/css/app-rtl.css') }}" id="maincss" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
        @yield('styles')
    </head>

    <body>
        <div class="wrapper">
            <!-- top navbar-->
            @include('layouts.includes.header')
            <!-- sidebar-->
            @include('layouts.includes.sidebar')
            <!-- offsidebar-->
            @include('layouts.includes.offsidebar')
            <!-- Main section-->
            <section class="section-container">
                <!-- Page content-->
                <div class="content-wrapper">
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>Error!</strong>
                            {{$errors->first()}}
                        </div>
                    @endif
                    @yield('content')
                </div>
            </section>
            <!-- Page footer-->
            @include('layouts.includes.footer')
        </div>
        @yield('body-area')
        <!-- =============== VENDOR SCRIPTS ===============-->
        <script src="{{ asset('/public/js/manifest.js') }}"></script>
        <script src="{{ asset('/public/js/vendor.js') }}"></script>
        <script src="{{ asset('/public/js/app.js') }}"></script>
        <!-- =============== APP SCRIPTS ===============-->
        <!-- =============== CUSTOM PAGE SCRIPTS ===============-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        @yield('scripts')
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <script defer src="{{ asset('/public/js/custom.js') }}"></script>
        
    </body>
</html>
