<div class="p-3 text-center">
    <span class="mr-2">&copy; 2020</span>
    <span class="mr-2">-</span>
    <span>{{env('APP_NAME')}}</span>
    <br />
    <span>{{env('APP_NAME')}} Admin Panel</span>
</div>
