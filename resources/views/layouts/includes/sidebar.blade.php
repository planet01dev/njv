<aside class="aside-container">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav class="sidebar" data-sidebar-anyclick-close="{{ isset($sidebarMenu) ? sizeof($sidebarMenu) : ''}}">
            <!-- START sidebar nav-->
            <ul class="sidebar-nav">
                <!-- START user info-->
                <li class="has-user-block">
                    <div class="collapse" id="user-block">
                        <div class="item user-block">
                            <!-- User picture-->
                            <div class="user-block-picture">
                                <div class="user-block-status">
                                    <img class="img-thumbnail rounded-circle" src="{{asset('public/img/logo.png') }}" alt="Avatar" width="60" height="60" />
                                    <!-- <div class="circle bg-success circle-lg"></div> -->
                                </div>
                            </div>
                            <!-- Name and Job-->
                            <div class="user-block-info">
                                <span class="user-block-name">Hello, {{Auth::user()->name}}</span>
                                <!-- <span class="user-block-role">Designer</span> -->
                            </div>
                        </div>
                    </div>
                </li>
                <!-- END user info-->
                 <li class="nav-heading ">
                    <span >Main Navigation</span>
                </li>
                <li class="{{ \Request::is('admin.dashboard.manage') || \Request::is('admin.dashboard.manage') ? ' active' : '' }}">
                    <a href="{{ route('admin.dashboard.manage') }}" title="Dashboard">
                        <em class="icon-speedometer"></em>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ \Request::is('admin.user.manage') || \Request::is('admin.device_history.manage') ? ' active' : '' }}">
                    <a href="{{ route('admin.user.manage') }}" title="User Management">
                        <em class="icon-user"></em>
                        <span>User Management</span>
                    </a>
                </li>
                
                <li class="{{ (\Request::is('admin.device.manage')) ? ' active' : '' }}">
                    <a href="#device_managment" title="Device" data-toggle="collapse" class="collapsed" aria-expanded="false">
                        <em class="icon-layers"></em>
                        <span data-localize="sidebar.nav.DASHBOARD">Device Management</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="device_managment" style="">
                        <li class="sidebar-subnav-header">Device</li>
                        <li class="">
                            <a href="{{ route('admin.device.manage') }}" title="Device">
                                <span data-localize="">Device</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('admin.device_history.manage')  }}" title="Device History">
                                <span data-localize="">Device History</span>
                            </a>
                        </li>


                    </ul>
                </li>
                <li class="{{ \Request::is('admin.city.manage') || \Request::is('admin.city.manage') ? ' active' : '' }}">
                    <a href="{{ route('admin.city.manage') }}" title="City">
                        <em class="icon-equalizer"></em>
                        <span>City</span>
                    </a>
                </li>
                <li class="{{ \Request::is('admin.school.manage') || \Request::is('admin.school.manage') ? ' active' : '' }}">
                    <a href="{{ route('admin.school.manage') }}" title="School">
                        <em class="icon-note"></em>
                        <span>School</span>
                    </a>
                </li>

               
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>