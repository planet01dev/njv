@extends('layouts.user')
@section('content')
    <div class="block-center mt-4 wd-xl">
        <!-- START card-->
        <div class="card card-flat">
            <div class="card-header text-center bg-dark">
                <a href="#"><img class="block-center rounded" src="{{asset('public/img/logo.png')}}" alt="Image" /></a>
            </div>
             <div class="card-body">
                    <p class="text-center py-2">SIGN IN TO CONTINUE.</p>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> -->

                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 @error('email') is-invalid @enderror" id="exampleInputEmail1" type="email" name="email" value="{{ old('email') }}"  placeholder="Enter email" autocomplete="off" required="required" />
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-envelope"></em></span>
                                </div>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 @error('password') is-invalid @enderror" id="exampleInputPassword1" type="password" name="password"  placeholder="Password" required="required" />
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-lock"></em></span>
                                </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>

                        <div class="clearfix">
                            <!-- <div class="checkbox c-checkbox float-left mt-0">
                                <label>
                                    <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                    <span class="fa fa-check"></span>
                                    Remember Me
                                </label>
                            </div>
                            @if(Route::has('password.request'))
                                <div class="float-right">
                                    <a class="text-muted" href="{{ route('password.request') }}">Forgot your password?</a>
                                </div>
                            @endif -->
                        </div>
                        <button class="btn btn-block btn-primary mt-3" type="submit">Login</button>
                    </form>
                    <!-- <p class="pt-3 text-center">Need to Signup?</p>
                    <a class="btn btn-block btn-secondary" href="/register">Register Now</a> -->

                        
                </div>
        </div>
        <!-- END card-->
        @include("layouts.includes.footer-page")
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/public/js/pages.js') }}"></script>
@endsection
